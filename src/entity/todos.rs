use sea_orm::entity::prelude::*;
use serde::{Serialize, Deserialize};

#[derive(Clone, Debug, PartialEq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "todos")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: u32, 
    pub title: Option<String>,
    pub body: Option<String>,
    pub inserted_at: DateTime,
    pub updated_at: DateTime,
    pub user_id: Option<i32>,
    pub category_id: Option<i32>,
    pub done: Option<i8>,
}

#[derive(Copy, Clone, Debug, EnumIter)]
pub enum Relation {}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        panic!("No RelationDef")
    }
}

impl ActiveModelBehavior for ActiveModel {}

#[derive(Clone, Debug, Default)]
pub struct TodosQuery {
    pub id: Option<u32>,
    pub title: Option<String>,
    pub user_id: Option<i32>,
    pub category_id: Option<i32>,
    pub done: Option<i32>,
}

/*
impl Default for TodosQuery {
    fn default() -> Self {
        Self {
            id: None,
            title: None,
            user_id: None,
            category_id: None,
            done: None,
        }
    }
}
*/

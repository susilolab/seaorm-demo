#![allow(unused_variables, dead_code, unused_imports)]
use sea_orm::{
    ConnectOptions,
    Database, QueryFilter,
    entity::*,
    query::*,
};
use sea_orm::entity::prelude::*;
use std::time::Duration;
use std::env;
use dotenv::dotenv;
use log::info;
use chrono::Local;

mod entity;

use entity::todos::{self, Entity as Todos, TodosQuery};
use entity::demo::{self, Entity as Demo};
use entity::users::{self, Entity as User};
use entity::user_group::{self, Entity as UserGroup};

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    env::set_var("RUST_LOG", "info");
    env_logger::init();
    dotenv().ok();

    let mut opt = ConnectOptions::new(env::var("DATABASE_URL")?);
    opt.max_connections(100)
        .min_connections(5)
        .connect_timeout(Duration::from_secs(8))
        .idle_timeout(Duration::from_secs(8))
        .sqlx_logging(true);

    let db = Database::connect(opt).await?;
    // insert_todos(&db).await?; 
    find_all_todos(&db).await?;
    find_one(&db).await?;
    find_by_id(&db, 100).await?;
    find_by_condition(&db).await?;
    find_filter_id(&db).await?;
    // let _ = insert_todos(&db).await?;
    // search_todos(&db).await?;
    // update_demo(&db).await?;
    // update_todos(&db, 25, "test update sea_orm".to_string(), "sea orm update data".to_string(), 1).await?;
    // update_todos_opt(&db, 25, Some("test update pake option".to_string()), None, None).await?;
    // find_all_users(&db).await?;
    // paginate_users(&db, 1u64).await?;
    // paginate_users(&db, 2u64).await?;
    // paginate_cond_users(&db, 1u64, Some("us".to_string())).await?;

    Ok(())
}

async fn find_all_users(db: &DatabaseConnection) -> anyhow::Result<()> {
    info!("--Find all users--");
    let rows: Vec<(users::Model, Option<user_group::Model>)> = User::find().find_also_related(UserGroup).all(db).await.unwrap();
    for (user, group) in rows {
        println!("user: {:?}", user);
        println!("user_group: {:?}", group);
    }

    Ok(())
}

async fn paginate_cond_users(db: &DatabaseConnection, page: u64, username: Option<String>) -> anyhow::Result<()> {
    info!("--Paginate cond users--");
    let mut cond = Condition::all();
    if username.is_some() {
        cond = cond.add(entity::users::Column::Username.contains(&username.unwrap()));
    }
    let paginator = User::find().filter(cond)
        .paginate(db, 20u64);
    let num_pages = paginator.num_pages().await.unwrap();
    let rows = paginator.fetch_page(page).await.unwrap();
    println!("num_pages: {}", num_pages);

    // for user in rows {
        println!("user: {:?}", rows);
    // }

    Ok(())
}

async fn paginate_users(db: &DatabaseConnection, page: u64) -> anyhow::Result<()> {
    info!("--Paginate users--");
    let paginator = User::find().find_also_related(UserGroup)
        .paginate(db, 2u64);
    let num_pages = paginator.num_pages().await.unwrap();
    let rows = paginator.fetch_page(page).await.unwrap();
    println!("num_pages: {}", num_pages);

    for (user, group) in rows {
        println!("user: {:?}", user);
        println!("user_group: {:?}", group);
    }

    Ok(())
}

async fn find_all_todos(db: &DatabaseConnection) -> anyhow::Result<()> {
    info!("--Find all todos--");
    let todos: Vec<entity::todos::Model> = Todos::find().all(db).await?;
    for val in todos {
        println!("id: {}, title: {}, body: {}", val.id, val.title.unwrap(), val.body.unwrap());
    }

    Ok(())
}

async fn find_one(db: &DatabaseConnection) -> anyhow::Result<()> {
    println!("--Find one todos--");
    let todos = Todos::find().one(db).await?;
    println!("{:?}", todos);

    Ok(())
}

async fn find_by_id(db: &DatabaseConnection, id: u32) -> anyhow::Result<()> {
    println!("--Find by id todos--");
    let todos = Todos::find_by_id(id).one(db).await?;
    println!("{:?}", todos);

    Ok(())
}

async fn find_by_condition(db: &DatabaseConnection) -> anyhow::Result<()> {
    println!("--Find by conditions todos--");
    let mut cond = Condition::all();
    cond = cond.add(entity::todos::Column::Title.contains("sqlx"));
    let mut todo = Todos::find().filter(cond);

    let limit = 3;
    if limit > 2 {
        todo = todo.limit(limit);
    }
    let rows = todo.all(db).await?;
    for val in rows {
        println!("id: {}, title: {}, body: {}", val.id, val.title.unwrap(), val.body.unwrap());
    }

    Ok(())
}

async fn find_filter_id(db: &DatabaseConnection) -> anyhow::Result<()> {
    println!("--Find filter id--");
    let mut cond = Condition::all();
    cond = cond.add(entity::todos::Column::Id.eq(1));
    let todo = Todos::find().filter(cond).one(db).await?;
    println!("{:?}", todo);

    Ok(())
}

async fn search_todos(db: &DatabaseConnection) -> anyhow::Result<()> {
    println!("--Search todos--");
    let params = TodosQuery {
        title: Some("sea_orm".to_owned()),
        done: Some(0),
        ..Default::default()
    };

    let mut cond = Condition::all();
    if let Some(val) = params.title {
        cond = cond.add(entity::todos::Column::Title.contains(&val));
    }

    if let Some(val) = params.done {
        cond = cond.add(entity::todos::Column::Done.eq(val));
    }
    let rows = Todos::find().filter(cond).all(db).await?;
    for val in rows {
        println!("id: {}, title: {}, body: {}", val.id, val.title.unwrap(), val.body.unwrap());
    }

    Ok(())
}

async fn insert_todos(db: &DatabaseConnection) -> anyhow::Result<()> {
    println!("--Insert todos--");
    let now = Local::now().naive_local();
    let data = entity::todos::ActiveModel {
        title: Set(Some("Belajar chrono date time".to_owned())),
        body: Set(Some("Belajar chrono date time".to_owned())),
        inserted_at: Set(now),
        updated_at: Set(now),
        user_id: Set(Some(1)),
        category_id: Set(Some(1)),
        done: Set(Some(0)),
        ..Default::default()
    };

    let row = Todos::insert(data).exec(db).await?;
    print!("row: {:?}", &row);
    print!("last insert id: {}", &row.last_insert_id);

    Ok(())
}

async fn update_todos(db: &DatabaseConnection, id: u32, title: String, body: String, done: i8) -> anyhow::Result<()> {
    println!("--Update todos--");
    let row = Todos::find_by_id(id)
        .one(db)
        .await?;
    let mut todo: todos::ActiveModel = row.unwrap().into(); 

    let now = Local::now().naive_local();
    todo.title = Set(Some(title));
    todo.body = Set(Some(body));
    todo.updated_at = Set(now);
    todo.done = Set(Some(done));

    let res: todos::Model = todo.update(db).await?;
    print!("todos: {:?}", res);

    Ok(())
}

async fn update_todos_opt(db: &DatabaseConnection, id: u32, title: Option<String>, body: Option<String>, done: Option<i8>) -> anyhow::Result<()> {
    println!("--Update todos versi lain--");
    let row = Todos::find_by_id(id)
        .one(db)
        .await?;
    let mut todo: todos::ActiveModel = row.unwrap().into(); 

    let now = Local::now().naive_local();
    todo.title = Set(title);
    todo.body = if body.is_some() {
        Set(body)
    } else {
        NotSet
    };
    todo.updated_at = Set(now);
    todo.done = if done.is_some() {
        Set(done)
    } else {
        NotSet
    };

    let res: todos::Model = todo.update(db).await?;
    print!("todos: {:?}", res);

    Ok(())
}

async fn update_demo(db: &DatabaseConnection) -> anyhow::Result<()> {
    println!("--Update demo--");
    let row = Demo::find_by_id(1)
        .one(db)
        .await?;
    let mut demo_row: demo::ActiveModel = row.unwrap().into(); 
    demo_row.cnt = Set(10);
    let demo_row: demo::Model = demo_row.update(db).await?;
    println!("{:?}", demo_row);

    Ok(())
}
